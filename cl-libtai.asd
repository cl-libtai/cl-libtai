(defsystem "cl-libtai"
  :description "TAI - Temps Atomique International; time conversion library"
  :version "0.51"
  :author "Lester Vecsey"
  :licence "LGPL"
  :components ((:file "cl-libtai")))
