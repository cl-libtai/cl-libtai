(in-package "COMMON-LISP-USER")

(defpackage #:cl-test-libtai
  (:use #:common-lisp #:cl-libtai))

(in-package "CL-TEST-LIBTAI")

(defvar test-libtai-iit (lisp-implementation-type))

(defun report-result (result form)
  (format t "~:[FAIL~;pass~] ... ~a~%" result form))

(defmacro check (form)
  `(report-result ,form ',form))

(defmacro check (&body forms)
  `(progn
     ,@(loop for f in forms collect `(report-result ,f ',f))))

(defun test-report()
    (format t "lisp-implementation-type: ~a~%" test-libtai-iit))

(defun test-leapsecs-add()
  (check
    (= 4611686019090075928 (cdr (assoc 'cl-libtai::x (leapsecs-add (make-tai64-internal 4611686019090075913) 0 *leapsecs*))))
    (= 4611686019090075928 (cdr (assoc 'cl-libtai::x (leapsecs-add (make-tai64-internal 4611686019090075913) 1 *leapsecs*))))
    (= 4611686019090075930 (cdr (assoc 'cl-libtai::x (leapsecs-add (make-tai64-internal 4611686019090075914) 0 *leapsecs*))))
    (= 4611686019090075929 (cdr (assoc 'cl-libtai::x (leapsecs-add (make-tai64-internal 4611686019090075914) 1 *leapsecs*))))
    (= 4611686019090075931 (cdr (assoc 'cl-libtai::x (leapsecs-add (make-tai64-internal 4611686019090075915) 0 *leapsecs*))))
    (= 4611686019090075931 (cdr (assoc 'cl-libtai::x (leapsecs-add (make-tai64-internal 4611686019090075915) 1 *leapsecs*))))))

(defun test-bench()
  (let ((full #x4000000043a72b922e8da9282313e8a8) (full-s "4000000043a72b922e8da9282313e8a8")
	(short #x4000000043a72b92)
	(short-s "4000000043a72b92"))
    (check
     (format t "t.x = ~a~%" (cdr (assoc 'x (make-tai64-internal (floor (/ full (expt 2 64)))))))
    (tai-unpack short-s)
    (taia-unpack full-s)
    (caltime-utc (make-tai64-internal short))
    (caltime-utc (tai-now))
    (destructuring-bind (&key year month day) (caldate-scan "+2005-12-26")
      (= year 2005) (= month 12) (= day 26))
    (leapsecs-gen (list "+1998-12-31" "+2005-12-31"))
    (= 53730 (caldate-mjd (make-caldate 2005 12 26)))
    ; MJD 51604 is year 5, day 0.
    (destructuring-bind (&key year month day) (caldate-frommjd 51604)
      (= year 2000) (= month 3) (= day 1)))))

(defun test-caltime-tai()
  (caltime-tai (make-caltime (make-caldate 2005 12 28) 3 36 10 0)))

(defun test-libtai()
  (progn (test-report) (test-leapsecs-add) (test-bench) (test-caltime-tai)))

