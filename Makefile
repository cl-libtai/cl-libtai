sp = cl-libtai/

backup:
	(cd .. ; tar cf - $(sp)/cl-libtai.asd $(sp)/cl-libtai.lisp | gzip -9 > $(sp)/cl-libtai.tar.gz)

clean:
	rm *.fas *.lib *.*~